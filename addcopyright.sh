#!/bin/sh
# -copyright-
#-# Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
#-#    Vincent Renardias, Willem Vermin
#-# 
#-# Permission is hereby granted, free of charge, to any person 
#-# obtaining a copy of this software and associated documentation 
#-# files (the “Software”), to deal in the Software without 
#-# restriction, including without limitation the rights to use, 
#-# copy, modify, merge, publish, distribute, sublicense, and/or 
#-# sell copies of the Software, and to permit persons to whom 
#-# the Software is furnished to do so, subject to the following 
#-# conditions:
#-# 
#-# The above copyright notice and this permission notice shall 
#-# be included in all copies or substantial portions of the Software.
#-# 
#-# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
#-# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
#-# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#-# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#-# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#-# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#-# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
#-# OTHER DEALINGS IN THE SOFTWARE.
#-# 
crfile=`mktemp`
cat << eof | sed 's/^/#-# /' > "$crfile"
Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
   Vincent Renardias, Willem Vermin

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the “Software”), to deal in the Software without 
restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or 
sell copies of the Software, and to permit persons to whom 
the Software is furnished to do so, subject to the following 
conditions:

The above copyright notice and this permission notice shall 
be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
OTHER DEALINGS IN THE SOFTWARE.

eof
n=0
while [ "$1" ] ; do
   f="$1"
   shift
   txt="-""copyright-"
   if file --mime "$f" | grep -q binary ; then
      echo "$f: binary"
      continue
   fi
   if ! grep -q -- "$txt" "$f" ; then 
      echo "$f: no $txt"
      continue
   fi
   # following 2 sed commands take care that only the first '-copyright-'
   # will be followed by the copyright text
   # Notice that sed requires a newline after the filename of the 'r' command
   sed -i "/^\s*#-#/d" "$f"
   sed -i "/$txt/{r $crfile
   :a;n;ba;}" "$f"
   n=`expr $n + 1`
done
echo "$n files copyrighted"
