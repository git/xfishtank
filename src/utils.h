/* -copyright-
#-# Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
#-#    Vincent Renardias, Willem Vermin
#-# 
#-# Permission is hereby granted, free of charge, to any person 
#-# obtaining a copy of this software and associated documentation 
#-# files (the “Software”), to deal in the Software without 
#-# restriction, including without limitation the rights to use, 
#-# copy, modify, merge, publish, distribute, sublicense, and/or 
#-# sell copies of the Software, and to permit persons to whom 
#-# the Software is furnished to do so, subject to the following 
#-# conditions:
#-# 
#-# The above copyright notice and this permission notice shall 
#-# be included in all copies or substantial portions of the Software.
#-# 
#-# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
#-# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
#-# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#-# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#-# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#-# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#-# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
#-# OTHER DEALINGS IN THE SOFTWARE.
#-# 
*/
#pragma once

//#define add_to_mainloop(prio,time,func,datap) g_timeout_add_full(prio,(int)1000*(time),(GSourceFunc)func,datap,0)


#include <stdio.h>
#include <X11/Intrinsic.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>

#define myexp10f(x) (expf(2.3025850930f*x))
#define mylog10f(x) (0.4342944819f*logf(x))

extern ssize_t mywrite(int fd, const void *buf, size_t count);
extern float   fsignf(float x);
extern FILE   *HomeOpen(const char *file,const char *mode,char **path);
extern float   sq2(float x, float y);
extern float   sq3(float x, float y, float z);
extern void    my_cairo_paint_with_alpha(cairo_t *cr, double alpha);
extern int     RandInt(int maxint);
extern Window  Window_With_Name( Display *dpy, Window top, const char *name);
extern double  wallclock(void);
extern double  wallcl(void);
extern void    mystrncpy(char *dest, const char *src, size_t n);
extern void    rgba2color(GdkRGBA *c, char **s);

extern Pixel   Black, White;

extern int   is_little_endian(void);
extern int   sgnf(float x);
extern float fsgnf(float x);
extern void  ClearScreen(void);
