/* -copyright-
#-# Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
#-#    Vincent Renardias, Willem Vermin
#-# 
#-# Permission is hereby granted, free of charge, to any person 
#-# obtaining a copy of this software and associated documentation 
#-# files (the “Software”), to deal in the Software without 
#-# restriction, including without limitation the rights to use, 
#-# copy, modify, merge, publish, distribute, sublicense, and/or 
#-# sell copies of the Software, and to permit persons to whom 
#-# the Software is furnished to do so, subject to the following 
#-# conditions:
#-# 
#-# The above copyright notice and this permission notice shall 
#-# be included in all copies or substantial portions of the Software.
#-# 
#-# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
#-# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
#-# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#-# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#-# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#-# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#-# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
#-# OTHER DEALINGS IN THE SOFTWARE.
#-# 
 */

#pragma once

#define NUM_FISH   29 
#define NUM_FRAMES  2

#include "fishes/fish00a.xpm"
#include "fishes/fish00b.xpm"
#include "fishes/fish01a.xpm"
#include "fishes/fish01b.xpm"
#include "fishes/fish02a.xpm"
#include "fishes/fish02b.xpm"
#include "fishes/fish03a.xpm"
#include "fishes/fish03b.xpm"
#include "fishes/fish04a.xpm"
#include "fishes/fish04b.xpm"
#include "fishes/fish05a.xpm"
#include "fishes/fish05b.xpm"
#include "fishes/fish06a.xpm"
#include "fishes/fish06b.xpm"
#include "fishes/fish07a.xpm"
#include "fishes/fish07b.xpm"
#include "fishes/fish08a.xpm"
#include "fishes/fish08b.xpm"
#include "fishes/fish09a.xpm"
#include "fishes/fish09b.xpm"
#include "fishes/fish10a.xpm"
#include "fishes/fish10b.xpm"
#include "fishes/fish11a.xpm"
#include "fishes/fish11b.xpm"
#include "fishes/fish12a.xpm"
#include "fishes/fish12b.xpm"
#include "fishes/fish13a.xpm"
#include "fishes/fish13b.xpm"
#include "fishes/fish14a.xpm"
#include "fishes/fish14b.xpm"
#include "fishes/fish15a.xpm"
#include "fishes/fish15b.xpm"
#include "fishes/fish16a.xpm"
#include "fishes/fish16b.xpm"
#include "fishes/fish17a.xpm"
#include "fishes/fish17b.xpm"
#include "fishes/fish18a.xpm"
#include "fishes/fish18b.xpm"
#include "fishes/fish19a.xpm"
#include "fishes/fish19b.xpm"
#include "fishes/fish20a.xpm"
#include "fishes/fish20b.xpm"
#include "fishes/fish21a.xpm"
#include "fishes/fish21b.xpm"
#include "fishes/fish22a.xpm"
#include "fishes/fish22b.xpm"
#include "fishes/fish23a.xpm"
#include "fishes/fish23b.xpm"
#include "fishes/fish24a.xpm"
#include "fishes/fish24b.xpm"
#include "fishes/fish25a.xpm"
#include "fishes/fish25b.xpm"
#include "fishes/fish26a.xpm"
#include "fishes/fish26b.xpm"
#include "fishes/fish27a.xpm"
#include "fishes/fish27b.xpm"
#include "fishes/fish28a.xpm"
#include "fishes/fish28b.xpm"

static const char **fishes[] = 
{
    fish00a_xpm,
    fish00b_xpm,
    fish01a_xpm,
    fish01b_xpm,
    fish02a_xpm,
    fish02b_xpm,
    fish03a_xpm,
    fish03b_xpm,
    fish04a_xpm,
    fish04b_xpm,
    fish05a_xpm,
    fish05b_xpm,
    fish06a_xpm,
    fish06b_xpm,
    fish07a_xpm,
    fish07b_xpm,
    fish08a_xpm,
    fish08b_xpm,
    fish09a_xpm,
    fish09b_xpm,
    fish10a_xpm,
    fish10b_xpm,
    fish11a_xpm,
    fish11b_xpm,
    fish12a_xpm,
    fish12b_xpm,
    fish13a_xpm,
    fish13b_xpm,
    fish14a_xpm,
    fish14b_xpm,
    fish15a_xpm,
    fish15b_xpm,
    fish16a_xpm,
    fish16b_xpm,
    fish17a_xpm,
    fish17b_xpm,
    fish18a_xpm,
    fish18b_xpm,
    fish19a_xpm,
    fish19b_xpm,
    fish20a_xpm,
    fish20b_xpm,
    fish21a_xpm,
    fish21b_xpm,
    fish22a_xpm,
    fish22b_xpm,
    fish23a_xpm,
    fish23b_xpm,
    fish24a_xpm,
    fish24b_xpm,
    fish25a_xpm,
    fish25b_xpm,
    fish26a_xpm,
    fish26b_xpm,
    fish27a_xpm,
    fish27b_xpm,
    fish28a_xpm,
    fish28b_xpm,
};
